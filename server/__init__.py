from flask import Flask
from creating_default_db import create_default_db

app = Flask(__name__)
from server import views

create_default_db()