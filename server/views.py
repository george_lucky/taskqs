from flask import render_template, request, redirect
from server import app

from scheme.managers.cache_manager import CacheManager
from scheme.managers.tree_manager import TreeManager
from scheme.event_pool import EventPool
from creating_default_db import create_default_db


tree_manager = TreeManager()
cache_manager = CacheManager()
event_pool = EventPool()


@app.route('/')
def main():
    rendered_tree = None
    rendered_cache = None
    db_nodes = tree_manager.get_all_db()
    ch_nodes = cache_manager.get_all_cache()
    if db_nodes:
        rendered_tree = tree_manager.get_render_tree_object()
    if ch_nodes:
        rendered_cache = cache_manager.get_rendered_cache()

    html = render_template('index.html', rendered_tree=rendered_tree, ch_nodes=ch_nodes, rendered_cache=rendered_cache,
                           tree=db_nodes)
    return html


@app.route('/upload_cache', methods=['POST'])
def upload_cache():
    node_name = request.form.get('node')
    if not node_name:
        return redirect('/', code=302)
    instance = tree_manager.get_node_by_name(node_name)
    cache_manager.load_to_cache(instance)
    return redirect('/', code=302)


@app.route('/reset_cache')
def reset_cache():
    cache_manager.remove_all_cache()
    event_pool.clear()
    tree_manager.remove_db()
    create_default_db()
    return redirect('/', code=302)


@app.route('/apply_changes', methods=['GET', 'POST'])
def apply_changes():
    event_pool.get_all_events()
    event_pool.start()
    return redirect('/', code=302)


@app.route('/add_node', methods=['POST'])
def add_node():
    value = request.form.get('value')
    parent_name = request.form.get('node')
    if not parent_name:
        return redirect('/', code=302)
    if len(cache_manager.cached_tree.nodes) != 0:
        parent = cache_manager.get_node_from_cache_by_name(parent_name)
        new_node = cache_manager.add_new_node_to_cache(value=value, parent=parent)
        event_pool.add_event(tree_manager.add_to_tree, (new_node, ))
    return redirect('/', code=302)


@app.route('/delete_node', methods=['POST'])
def delete_node():
    node_name = request.form.get('node')
    if not node_name:
        return redirect('/', code=302)
    node = cache_manager.get_node_from_cache_by_name(node_name)
    cache_manager.delete_node_in_cache(node)
    event_pool.add_event(tree_manager.delete_node_by_name, (node_name, ))
    return redirect('/', code=302)


@app.route('/change_value', methods=['POST'])
def change_value():
    node_name = request.form.get('node')
    if not node_name:
        return redirect('/', code=302)
    value = request.form.get('value'+node_name)
    node = cache_manager.get_node_from_cache_by_name(node_name)
    node.will_be_changed = value
    if value == '':
        return redirect('/', code=302)
    event_pool.add_event(tree_manager.update_node_by_name, (node_name, value))
    return redirect('/', code=302)
