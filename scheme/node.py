from anytree import Node
from singleton_decorator import singleton


@singleton
class TreeScheme:
    """TreeScheme is class for containing all nodes."""
    def __init__(self, nodes: list = None):
        if not nodes:
            self.nodes = []
        else:
            self.nodes = nodes

    def return_all_nodes(self) -> list:
        """Returns all nodes from Tree"""
        return self.nodes


class INode(Node):
    NODE_ID = int()

    def __init__(self, value: str, parent=None):
        super(INode, self).__init__(value, parent)
        INode.NODE_ID += 1
        self._id = INode.NODE_ID
        self.value = value
        self.name = 'Node' + str(INode.NODE_ID)
        self.is_deleted = False
        self.will_be_deleted = False
        self.will_be_changed = self.value
        self.descendants_in_cache = set()

    def __str__(self):
        return f'Node name: {self.name}. With value: {self.value}'

    def delete_children(self) -> None:
        """Deleting all children of INode object"""
        for child in self.descendants:
            child.is_deleted = True
