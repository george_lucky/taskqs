from singleton_decorator import singleton


@singleton
class CacheScheme:
    """CacheScheme is class for containing all cached nodes"""
    def __init__(self, nodes: list=None):
        if not nodes:
            self.nodes = []
        else:
            self.nodes = nodes
