from anytree import RenderTree, ContRoundStyle

from scheme.cache import CacheScheme
from scheme.node import INode


class CacheManager:
    """Manager which manages all operations with Cache"""
    def __init__(self):
        self.cached_tree = CacheScheme()

    def load_to_cache(self, node: INode) -> CacheScheme:
        """Method adds node to cache"""
        node.descendants_in_cache = set(descendant for descendant in node.descendants
                                        if descendant in self.cached_tree.nodes)
        self.delete_if_ancestor_deleted(node)
        self.update_cached_nodes_data(node)
        self.cached_tree.nodes.append(node)
        return self.cached_tree

    def delete_if_ancestor_deleted(self, node: INode):
        """Delete node if node's ancestor already deleted"""
        nodes_in_cache = set(self.get_all_cache())
        nodes_in_cache.intersection_update(node.path)
        for ancestor in nodes_in_cache:
            if ancestor.will_be_deleted:
                node.will_be_deleted = True
                break

    def delete_node_in_cache(self, node) -> None:
        """Deletes node and node's children in cache"""
        node.will_be_deleted = True
        if node.is_leaf:
            return

        for c_node in node.descendants_in_cache:
            c_node.will_be_deleted = True

    def add_new_node_to_cache(self, value: str, parent: INode) -> INode:
        """Creates and adds new node to cache"""
        instance = INode(value, parent=parent)
        parent.descendants_in_cache.add(instance)
        for ch_node in self.cached_tree.nodes:
            if parent in ch_node.descendants_in_cache:
                ch_node.descendants_in_cache.add(instance)
        self.cached_tree.nodes.append(instance)
        return instance

    def get_node_from_cache_by_name(self, node_name: str) -> INode or None:
        """Returns node from cache by name of node"""
        instance = next((nd for nd in self.cached_tree.nodes if nd.name == node_name), None)
        return instance

    def update_cached_nodes_data(self, node):
        for ch_node in self.cached_tree.nodes:
            if ch_node in node.path:
                ch_node.descendants_in_cache.add(node)

    def get_all_cache(self) -> list:
        """Returns all nodes from cache"""
        return self.cached_tree.nodes

    def remove_all_cache(self) -> None:
        """Clears the cache from nodes"""
        self.cached_tree.nodes = []

    def get_rendered_cache(self) -> RenderTree:
        """Returns RenderTree object to display the tree"""
        return RenderTree(self.get_all_cache()[0].root, ContRoundStyle)
