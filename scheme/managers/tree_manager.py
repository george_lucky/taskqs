from anytree import RenderTree, ContRoundStyle

from scheme.node import INode, TreeScheme


class TreeManager:
    """Manager which manages INode and TreeScheme objects.
        CRUD operations for Nodes, printing full tree_scheme."""

    def __init__(self):
        self.tree_scheme = TreeScheme(nodes=[])

    def get_node_by_name(self, node_name: str) -> INode or None:
        """Returns node from tree by name of node"""
        instance = next((nd for nd in self.tree_scheme.nodes if nd.name == node_name), None)
        return instance

    def create_node(self, value: str, parent_name: str = None) -> INode:
        """Creates and returns new node. Adds new node to tree"""
        parent = self.get_node_by_name(parent_name)
        instance = INode(value=value, parent=parent)
        self.add_to_tree(instance)
        return instance

    def add_to_tree(self, node: INode) -> None:
        """Adds node to tree"""
        self.tree_scheme.nodes.append(node)

    def update_node_by_name(self, node_name: str, value: str) -> INode or None:
        """Updates value of node and returns instance of INode or None if node not found"""
        instance = self.get_node_by_name(node_name)
        if instance:
            instance.value = value
            return instance

    def delete_node_by_name(self, node_name: str) -> INode or None:
        """Deletes node by name and returns instance of INode if node was found otherwise None"""
        instance = self.get_node_by_name(node_name)
        if instance:
            instance.is_deleted = True
            instance.delete_children()
        return instance

    def get_all_db(self) -> list:
        """Returns all nodes from tree"""
        return self.tree_scheme.nodes

    def get_render_tree_object(self) -> RenderTree:
        """Returns RenderTree object to display the tree"""
        return RenderTree(self.tree_scheme.nodes[0], ContRoundStyle)

    def remove_db(self):
        """Removes all db"""
        INode.NODE_ID = 0
        self.tree_scheme.nodes = []
