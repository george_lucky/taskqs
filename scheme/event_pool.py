from singleton_decorator import singleton


@singleton
class EventPool:
    """EventPool is the pool of the events.
    Event is the dict which contains the method as key
    and arguments for the method as value"""
    def __init__(self, events: list=[]):
        self.events = events

    def add_event(self, method, args=()) -> None:
        """Method that adds an event to the pool"""
        event = {method: args}
        self.events.append(event)

    def start(self) -> None:
        """Method that starts all events and clears the pool"""
        for event in self.events:
            method = list(event.keys())[0]
            args = list(event.values())[0]
            method(*args)

        self.clear()

    def get_all_events(self) -> list:
        """Returns all events from the pool"""
        return self.events

    def clear(self) -> None:
        """Cleans the pool from events"""
        self.events = []
