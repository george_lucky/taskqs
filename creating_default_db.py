from scheme.managers.tree_manager import TreeManager
from scheme.event_pool import EventPool


def create_default_db():
    tree_manager = TreeManager()
    event_pool = EventPool()
    event_pool.add_event(tree_manager.create_node, args=('Value1', ))
    event_pool.add_event(tree_manager.create_node, args=('Value2', 'Node1'))
    event_pool.add_event(tree_manager.create_node, args=('Value3', 'Node1'))
    event_pool.add_event(tree_manager.create_node, args=('Value4', 'Node1'))
    event_pool.add_event(tree_manager.create_node, args=('Value5', 'Node1'))
    event_pool.add_event(tree_manager.create_node, args=('Value6', 'Node3'))
    event_pool.add_event(tree_manager.create_node, args=('Value7', 'Node4'))
    event_pool.add_event(tree_manager.create_node, args=('Value8', 'Node5'))
    event_pool.add_event(tree_manager.create_node, args=('Value9', 'Node7'))
    event_pool.add_event(tree_manager.create_node, args=('Value10', 'Node8'))
    event_pool.add_event(tree_manager.create_node, args=('Value11', 'Node6'))
    event_pool.add_event(tree_manager.create_node, args=('Value12', 'Node9'))

    event_pool.start()


if __name__ == '__main__':
    create_default_db()
