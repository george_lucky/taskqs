import os
import logging


def start_logging():
    logger = logging.getLogger(__name__)
    logger.setLevel(logging.INFO)

    stream = logging.StreamHandler()
    logger.addHandler(stream)

    return logger


BASE_DIR = os.path.dirname(os.path.abspath(__name__))
STATIC_DIR = BASE_DIR + '/static/'
TEMPLATES_DIR = BASE_DIR + '/templates/'
CONFIG_PATH = BASE_DIR + '/server/'
