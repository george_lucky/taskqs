FROM python:3.6

MAINTAINER George Trofimencev "george98trofimencev@gmail.com"

RUN apt-get update -y

ADD . /code
WORKDIR  /code
RUN pip install -r requirements.txt
CMD FLASK_APP=server/run.py flask run --host 0.0.0.0 --port 5000
